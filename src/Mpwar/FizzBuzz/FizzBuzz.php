<?php
namespace Mpwar\FizzBuzz;

class FizzBuzz 
{
	const FIZZ_VALUE  = 'fizz';
	const BUZZ_VALUE  = 'buzz';
	const FIZZ_BUZZ_VALUE = 'fizz buzz';
	const NOT_FIZZ_OR_BUZZ_VALUE = 'x';
	const FIZZ_CASE_VALUE = 3;
	const BUZZ_CASE_VALUE  = 5;

	public function calcualteResults($inputValue){
		if($this->isFizzBuzz($inputValue)){
			return self::FIZZ_BUZZ_VALUE;
		}
		if ($this->isBuzz($inputValue)){
			return self::BUZZ_VALUE;
		}
		if ($this->isFizz($inputValue)){
			return self::FIZZ_VALUE;
		}
		return self::NOT_FIZZ_OR_BUZZ_VALUE;
	}

	private function isFizz($inputValue){
		return $inputValue % self::FIZZ_CASE_VALUE  == 0;
	}

	private function isBuzz($inputValue)
	{
		return $inputValue % self::BUZZ_CASE_VALUE == 0;
	}

	private function isFizzBuzz($inputValue)
	{
		return $this->isFizz($inputValue) && $this->isBuzz($inputValue);
	}
}