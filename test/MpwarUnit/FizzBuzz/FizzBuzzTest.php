<?php

namespace MpwarUnit\FizzBuzz;

use Mpwar\FizzBuzz\FizzBuzz;
use PHPUnit_Framework_TestCase;

final class FizzBuzzTest extends PHPUnit_Framework_TestCase
{
	const FIZZ_VALUE = 'fizz';
	const BUZZ_VALUE = 'buzz';
	const FIZZ_BUZZ_VALUE = 'fizz buzz';
	const NOT_FIZZ_OR_BUZZ_VALUE = 'x';
	
	/**
   * @test
   * @dataProvider fizzCasesProvider
   */
	public function shouldReturnFizzIfNumberIsDivisibleByThree($inputValue){
		$this->assertEquals(self::FIZZ_VALUE,$this->calcualteResults($inputValue));
	}

	/**
   * @test
   * @dataProvider buzzCasesProvider
   */
	public function shouldReturnBuzzIfNumberIsDivisibleByFive($inputValue){
		$this->assertEquals(self::BUZZ_VALUE,$this->calcualteResults($inputValue));
	}

	/**
   * @test
   * @dataProvider fizzBuzzCasesProvider
   */
	public function shouldReturnFizzBuzzIfNumberIsDivisibleByThreeAndFive($inputValue){
		$this->assertEquals(self::FIZZ_BUZZ_VALUE,$this->calcualteResults($inputValue));
	}
	/**
   * @test
   * @dataProvider notFizzOrBuzzCasesProvider
   */
	public function shouldReturnXIfNumberIsNotDivisbleByThreeOrFive($inputValue){
		$this->assertEquals(self::NOT_FIZZ_OR_BUZZ_VALUE,$this->calcualteResults($inputValue));
	}

	public function fizzCasesProvider()
  {
      return [
          'caso del 3' => [
            'Input value' => 3
          ],
          'caso del 6' => [
          	'Input value'=> 6
          ],
          'caso del 9'=>[
          	'Input value'=> 9
          ],
      ];
  }

  public function buzzCasesProvider()
  {
      return [
          'caso del 5' => [
            'Input value' => 5
          ],
          'caso del 10' => [
          	'Input value'=> 10
          ],
          'caso del 25'=>[
          	'Input value'=> 25
          ],
      ];
  }

  public function fizzBuzzCasesProvider()
  {
      return [
          'caso del 15' => [
            'Input value' => 15
          ],
          'caso del 30' => [
          	'Input value'=> 30
          ],
          'caso del 60'=>[
          	'Input value'=> 60
          ],
      ];
  }

  public function notFizzOrBuzzCasesProvider()
  {
      return [
          'caso del 1' => [
            'Input value' => 1
          ],
          'caso del 11' => [
          	'Input value'=> 11
          ],
          'caso del 13'=>[
          	'Input value'=> 13
          ],
      ];
  }

  private function calcualteResults($inputValue){
  	$fizzBuzz = new FizzBuzz;
  	return $fizzBuzz->calcualteResults($inputValue); 
  }
}